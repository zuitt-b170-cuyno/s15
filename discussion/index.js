// Mathematical Operators
/* 
    + sum
    - difference
    * product
    / division
    % modulus
    += addition
    -= subtraction
    *= multiplication
    /= division
    %= reminder
*/

function mod() {
    // return 9 + 2
    // return 9 - 2
    // return 9 * 2
    // return 9 / 2
    // return 9 % 2
    let x = 10
    // return x += 2
    // return x -= 2
    // return x *= 2
    // return x /= 2
    return x %= 2
}

console.log(mod())

// Assignment Operator - assign value to variable

let x = 1
let sum = 1
x -= 1
sum += 1

console.log(sum)
console.log(x)

// Increment and Decrement
// Pre-increment - add 1 before assigning the result to a variable
// Post-increment - add 1 after assigning the result to a variable
let z = 1
let increment = ++z

console.log(`Result of Pre-increment: ${z}`)
console.log(`Result of Pre-increment: ${increment}`)

increment = z++
console.log(`Result of Post-increment: ${z}`)
console.log(`Result of Post-increment: ${increment}`)

// Pre-decrement - substract 1 before assigning the result to a variable
// Post-decrement - substract 1 after assigning the result to a variable
let decrement = --z

console.log(`Result of Pre-decrement: ${z}`)
console.log(`Result of Pre-decrement: ${decrement}`)

decrement = z--
console.log(`Result of Post-decrement: ${z}`)
console.log(`Result of Post-decrement: ${decrement}`)

// Comparison Operator
/* 
    == equal to
    === strict equality
    > less than
    < greater than
    >= greter than or equal to
    <= less than or equal to
    != not equal to
    !== strict inequality
*/

let juan = "juan"
console.log(juan == "juan")
console.log(0 == false)
console.log(1 == 1)

console.log(1 === 1)
console.log(1 === "1")
console.log(1 != 1)
console.log(juan != "juan")
console.log(0 !== false)

// Logical Operator
/* 
    || OR operator - returns if at least one boolean expression is true; otherwise, it returns false
    && AND operator - returns if all boolean expressions are true; otherwise, it returns false
    ~ NOT operator - negate the value of boolean expression
*/

let isLegalAge = true, isRegistered = true
let allRequirementMet = isLegalAge && isRegistered

console.log(`Result of logical AND operator: ${allRequirementMet}`)

// IF Statement
// prompt is a dialog box plus input fields
// alert is a dialog box with no input fields. Used for warnings, announcement, etc. 
// parseInt() converts string to int
// parseFloat() converts string to float
// NaN - Not a Number
// parseInt(promt(...)) - Only accepts the first number substring

const num = -1
if (num < 0) 
    console.log("Negative")
else if (num > 0)
    console.log("Positive")
else
    console.log("Zero")

const number = parseInt(prompt("Enter number: "))
if (number >= 59) 
    alert("Senior Age")
else
    alert("Invalid Number")

console.log(number)
console.log(typeof number)

/*
	1. Quezon
	2. Valenzuela
	3. Pasig
	4. Taguig
*/

const city = parseInt(prompt("Enter a Number"));
if (city === 1) 
	alert("Welcome to Quezon City")
else if (city === 2)
	alert("Welcome to Valenzuela City")
else if ( city === 3 )
	alert("Welcome to Pasig City")
else if ( city === 4 )
	alert("Welcome to Taguig City")
else 
	alert("Invalid Number")

function determineTypoonIntensity(windspeed) {
    if (windspeed < 30)
        return "Not a typoon yet"
    else if (windspeed <= 61)
        return "Tropical Depression Detected"
    else if (windspeed >= 62 && windspeed <= 117)
        return "Severe Tropical Storm"
    else
        return "Typoon Detected"
}

const message = determineTypoonIntensity(70)
console.log(message)

// Ternary Operation - shorthand notation for if-else statement
let ternaryResult = 1 < 18 ? true : false
console.log(`Result of Ternary Operation: ${ternaryResult}`)

function isOfLegalAge(){
	return "You are of the legal age Limit"
};

function isUnderAge(){
	return "You are under the legal Age"
}

let age = parseInt(prompt("What is Your Age?"))
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge()

alert(`Result of Ternary Operator in Function: ${LegalAge}`)

// Switch Statement
const day = prompt("What day is it today").toLowerCase()
switch (day) {
    case "sunday":
        alert("The color of the day is red")
        break
    case "monday":
        alert("The color of the day is orange")
        break
    case "tuesday":
        alert("The color of the day is yellow")
        break
    case "wednesday":
        alert("The color of the day is green")
        break
    case "thursday":
        alert("The color of the day is blue")
        break
    case "friday":
        alert("The color of the day is violet")
        break
    case "saturday":
        alert("The color of the day is indigo")
        break
    default:
        alert("Please input a valid day")
}

// Try-Catch-Finally

function showIntensityAlert(windspeed) {
    let div
    try {
        // alert(determineTypoonIntensity(windspeed))
        div = 1 / 0
    }
    catch(error) {
        console.log(typeof error)
        console.warn(error.message)
    }
    finally {
        alert("Intensity updates will show new alert")
    }
}

showIntensityAlert(56)