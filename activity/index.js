console.log("Hello World")
const num1 = parseInt(prompt("Provide a number")), num2 = parseInt(prompt("Provide another number"))
const total = num1 + num2

// Line 6-18 will not print anything if at least one number is NULL
if (total < 10) 
    alert(`The sum of two numbers is: ${num2 + num1}`)
else if (total >= 10 && total <= 20)
    alert(`The difference of two numbers is: ${num2 - num1}`)
else if (total >= 21 && total <= 29)
    alert(`The product of two numbers is: ${num2 * num1}`)
else if (total >= 30)
    alert(`The quotient of two numbers is: ${num2 / num1}`)

if (total >= 10)
    alert(`Total: ${total}`)
else if (total < 10)
    console.warn(`Total: ${total}`)

const username = prompt("What is your name?"), age = parseInt(prompt("What is your age?"))
function isLegalAge(age) {
    age >= 18 ? alert("You are of legal age") : alert("You are not allowed here")
}

if (!username || !age) 
    alert("Are you a time traveler?")
if (username && age) 
    alert(`Hello, ${username}. Your age is ${age}`) 

try {
    isLegalag(age)
}
catch(error) {
    console.warn(error.message)
}
finally {
    isLegalAge(age)
}

switch (age) {
    case 18: 
        alert("You are now allowed to party.")
        break
    case 21: 
        alert("You are now part of the adult society.")   
        break
    case 65: 
        alert("We thank you for your contribution to society.")
        break
    default:
        alert("Are you sure you're not an alien?")
}